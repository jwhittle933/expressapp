var express = require('express');
var router = express.Router();

/* GET users listing. */
let users = {
  user1: {
    name: 'Jonathan Whittle',
    age: 28,
    street: '603 Royer Ct.',
    city: 'Louisville',
    state: 'Kentucky',
    languages: ['Hebrew', 'Aramaic', 'Syriac', 'Ugaritic', 'Greek', 'Latin', 'German', 'French', 'English'],
    programming: ['HTML', 'CSS', 'JavaScript', 'PHP', 'Python', 'C#'],
    frameworks: ['Vuejs', 'React', 'Nodejs', 'Express', 'Laravel', 'WordPress', 'ASP.NET']
  },
  user2: {

  },
  user3: {

  }
}

router.get('/', function(req, res, next) {
  res.render('users', {users});
});

module.exports = router;
